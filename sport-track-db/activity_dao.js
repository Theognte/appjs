var db = require(__dirname+'/./sqlite_connection');

var ActivityDAO = function(){
    this.insert = function(values, callback){
        let sql = ('INSERT INTO Activite ("idActivite", "date", "description", "distance", "heureDeb", "duree", "frequenceMin", "frequenceMoy", "frequenceMax", "emailUser") VALUES (?,?,?,?,?,?,?,?,?,?)');
        db.run(sql, values[0],values[1],values[2],values[3],1,2,values[4],values[5],values[6],values[7], function(err) {
            if (err) {
              return console.log(err.message);
            }
            console.log('A row has been inserted');
        });
    };
    this.update = function(key, values){
        this.delete(key);
        this.insert(values);
    };
    this.deleteAll = function(){

        db.run('DELETE FROM Activite', function(err) {
            if (err) {
                return console.log(err.message);
            }
            console.log('The database have been emptied.');
        });
    };
    this.delete = function(key){
        let sql = 'DELETE FROM Activite WHERE idActivite=? ';
        db.run(sql, key, function(err) {
            if (err) {
              return console.log(err.message);
            }
            console.log('A row has been removed');
        });
    };
    this.findAll = function(callback){
        let sql = 'SELECT * FROM Activite'
        db.all(sql,callback);
    };

    this.findByKey = function(key,callback){
        let sql = 'SELECT * FROM Activite WHERE emailUser=?';
        db.all(sql, key ,callback);

    };

    this.getId = function(callback) {
		db.get("SELECT MAX(idActivite) AS id FROM Activite", callback);
	};
};
var dao = new ActivityDAO();
module.exports = dao;