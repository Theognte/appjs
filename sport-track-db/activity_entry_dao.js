/** Écrivez, dans un fichier activity_entry_dao.js, une classe ActivityEntryDAO permettant d’ajouter, de modifier et de
 supprimer une donnée cardiaque et de position dans une activité. Cette classe devra également permettre de lister
 l’ensemble des données et de lister les données issues d’une activité particulière.*/

var db = require(__dirname+'/./sqlite_connection');


var ActivityEntryDAO = function(){

    this.insert = function(values){
        db.run('INSERT INTO Donnee("idData","time", "frequenceC", "latitude", "longitude", "altitude") VALUES (?,?,?,?,?,?)', values[0], values[1], values[2], values[3], values[4], values[5], function(err) {
            if (err) {
                return console.log(err.message);
            }
            console.log('A row has been inserted');
        });

    };

    this.deleteAll = function(){

        db.run('DELETE FROM Donnee', function(err) {
            if (err) {
                return console.log(err.message);
            }
            console.log('The database have been emptied.');
        });
    };

    this.update = function(key, values){

        this.delete(key);
        this.insert(values);
        console.log('The database have been updated.')

    };


    this.delete = function(key){

        db.run('DELETE FROM Donnee WHERE idData=?', key, function(err) {
            if (err) {
                return console.log(err.message);
            }
            console.log('The row number has been removed');
        });
    };

    this.findAll = function(callback){

        db.all('SELECT * FROM Donnee', callback);
    };

    this.findByKey = function(key, callback){
        db.all('SELECT * FROM Donnee where idData=?', key, callback);

    };
};

var dao = new ActivityEntryDAO();
module.exports = dao;