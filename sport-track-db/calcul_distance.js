var Calcul = function() {
    this.calculDistance2PointsGPS = function(lat1, long1, lat2, long2) {
        
        lat1 = Math.PI*lat1/180;
        lat2 = Math.PI*lat2/180;
        long1 = Math.PI*long1/180;
        long2 = Math.PI*long2/180;

        let d = 6378137*Math.acos(Math.sin(lat2)*Math.sin(lat1)+Math.cos(lat2)*Math.cos(lat1)*Math.cos(long2-long1));

        return d;
    }

    this.calculDistanceTrajet = function(activite) {
	
	    let total = 0;
    
        for (let i = 0; i < activite.length-1; i=i+1) {

            lat1 = activite[i]["latitude"];
            long1 = activite[i]["longitude"];
            lat2 = activite[i+1]["latitude"];
            long2 = activite[i+1]["longitude"];

            total = total + this.calculDistance2PointsGPS(lat1, long1, lat2, long2);
        }

        return total;
    }
}
var calcul = new Calcul();
module.exports = calcul;