const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database(__dirname+'/./db/sport_track.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
      return console.error(err.message);
    } else {
        console.log('Connected to the database.');
    }
  });

  module.exports = db;