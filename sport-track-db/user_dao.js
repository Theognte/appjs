var db = require(__dirname+'/./sqlite_connection');
var UserDAO = function(){
    this.insert = function(values,callback){
        let sql = ('INSERT INTO Utilisateur ("nom", "prenom", "dateN", "sexe", "poids", "taille", "email", "password") VALUES (?,?,?,?,?,?,?,?)');
        db.run(sql, values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7], function(err) {
            if (err) {
              return console.log(err.message);
            }
            console.log('A row has been inserted');
        });

    };
    this.update = function(key, values,callback){
        this.delete(key,callback);
        this.insert(values,callback);
    };
    this.deleteAll = function(){

        db.run('DELETE FROM Donnee', function(err) {
            if (err) {
                return console.log(err.message);
            }
            console.log('The database have been emptied.');
        });
    };
    this.delete = function(key){
        let sql = 'DELETE FROM Utilisateur WHERE email=? '      
        db.run(sql, key, function(err) {
            if (err) {
              return console.log(err.message);
            }
            console.log('A row has been removed');
        });
    };
    this.findAll = function(callback){
        let sql = 'SELECT * FROM Utilisateur'
        db.all(sql, callback);
    };
    this.findByKey = function(key,callback){
        let sql = 'SELECT * FROM Utilisateur WHERE email=?';
        db.all(sql, key, callback);

    };
};
var dao = new UserDAO();
module.exports = dao;