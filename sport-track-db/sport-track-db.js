var db_connection = require(__dirname+'/./sqlite_connection');
var user_dao = require(__dirname+'/./user_dao');
var activity_dao = require(__dirname+'/./activity_dao');
var activity_entry_dao = require(__dirname+'/./activity_entry_dao');
var calcul_distance = require(__dirname+'/././calcul_distance');

module.exports = {
    db_connection: db_connection,
     user_dao: user_dao, 
     activity_dao: activity_dao, activity_entry_dao: activity_entry_dao, calcul_distance: calcul_distance};