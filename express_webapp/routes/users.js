var express = require('express');
var router = express.Router();
var user_dao = require('../../sport-track-db/sport-track-db').user_dao;

router.get("/",(req, res) => {
  res.render('users');
});

router.post('/', (req, res) => {

  if (req.body.lname && req.body.fname && req.body.birthday && req.body.gender && req.body.height && req.body.weight && req.body.mail && req.body.mdp) {

    var firstname = req.body.fname;
    var lastname = req.body.lname;
    var date = req.body.birthday;
    var gender = req.body.gender;
    var height = parseInt(req.body.height, 10);
    var weight = parseInt(req.body.weight, 10);
    var email = req.body.mail;
    var password = req.body.mdp;
    var confirm = req.body.confmdp;

    if (confirm === password) {

      var user = [firstname, lastname, date, gender, weight, height, email, password];

      user_dao.findByKey(req.body.mail, function (err, rows) {

        if (err) {
          res.render("users");
        } else {
          user_dao.insert(user);
          res.redirect("/connect");
        }
      });
    } else {
      res.render("users", data={});
    }
  } else {
    res.render("users", data={});
  }
});

module.exports = router;