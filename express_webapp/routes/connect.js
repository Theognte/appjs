var express = require('express');
var router = express.Router();
var user_dao = require('../../sport-track-db/sport-track-db').user_dao;

router.get("/",(req, res) => {
  res.render('connect');
});

router.post('/', (req, res) => {

  var sess;

  if (req.body.mail && req.body.mdp) {

    var email = req.body.mail;
    var password = req.body.mdp;

    user_dao.findByKey(email, function (err, rows) {
      if (err) {
        res.render('connect');
      } else if (rows) {
        let valid = false;
        for (row of rows) {
          if (row.password === password) {
            valid = true;
          }
        }
        if (valid) {
          sess = req.session;
          sess.secret = email;
          res.redirect('/upload');
        } else {
          res.render('connect');
        }
      } else {
        res.render('connect');
      }
    })
  } else {
    res.render('connect')
  }
});

module.exports = router;