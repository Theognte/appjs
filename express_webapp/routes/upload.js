var express = require('express');
var uniqid = require('uniqid');
var router = express.Router();
var activity_dao = require('../../sport-track-db').activity_dao;
var activity_entry_dao = require('../../sport-track-db/sport-track-db').activity_entry_dao;
var calcul_distance = require('../../sport-track-db/sport-track-db').calcul_distance;

var formidable = require('formidable');
var fs = require('fs');

router.get('/', function(req, res, next) {
	if(req.session.secret !== undefined) {
        res.render('upload');
    } else {
        res.redirect('/');
    }
});

router.post('/', function(req, res, next) {
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		let jsfile = files.jsonfile.path;

		fs.readFile(jsfile, function (err, data) {
			if (err) {
				res.render('upload');
				console.error(err);
			}

			let json = JSON.parse(data);

			let idActivite = Math.floor(Math.random() * 1000);
			let date = json['activity']['date'];
			let description = json['activity']['description'];

			let distance = calcul_distance.calculDistanceTrajet(json['data']);

			if(req.session.secret !== undefined) {
				var frequenceMax = 0;
				var frequenceMin = 200;
				var frequenceMoy = 0;

				for (cardio of json['data']) {
					var c = cardio['cardio_frequency'];
					if (c < frequenceMin) {
						frequenceMin = c;
					}
					if (c > frequenceMax) {
						frequenceMax = c;
					}
					frequenceMoy = frequenceMoy + c;
				}

				frequenceMoy = frequenceMoy/(json['data'].length);

				activity_dao.insert([idActivite, date, description, distance, frequenceMin, frequenceMoy, frequenceMax, req.session.secret]);
				for (data of json['data']) {
					let idData = Math.floor(Math.random() * 1000);
					let time = data['time'];
					let frequenceC = data['cardio_frequency'];
					let latitude = data['latitude'];
					let longitude = data['longitude'];
					let altitude = data['altitude'];
					activity_entry_dao.insert([idData, time, frequenceC, latitude, longitude, altitude]);
				}
				res.redirect('/activities');
			}
		});
	});
});

module.exports = router;
