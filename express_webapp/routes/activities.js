var express = require('express');
var router = express.Router();
var activity_dao = require('../../sport-track-db/sport-track-db').activity_dao;

router.get('/', function(req, res, next) {
  if(req.session.secret !== undefined) {
    activity_dao.findByKey(req.session.secret, function(err, rows) {
      if(err != null) {
           console.log("ERROR= " + err);
         }else {
          res.render('activities', {data:rows});
     }
    })
  } else {
    res.redirect('/');
  };
});

module.exports = router;