var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var bodyParser = require('body-parser');
var users = require('./routes/users');
var upload = require('./routes/upload');
var activities = require('./routes/activities');
var connection = require('./routes/connect');
var disconnect = require('./routes/disconnect');
var session = require('express-session');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  secret: 'temporarySecretKey',
  resave: false,
  saveUninitialized: false,
}));

app.use('/', indexRouter);
app.use('/users', users);
app.use('/upload', upload);
app.use('/connect', connection);
app.use('/disconnect', disconnect);
app.use('/activities', activities);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
