class CalculDistance {
    calculDistance2PointsGPS(lat1, long1, lat2, long2) {
        
        lat1 = (Math.PI*lat1)/180;
        lat2 = (Math.PI*lat2)/180;
        long1 = (Math.PI*long1)/180;
        long2 = (Math.PI*long2)/180;

        d = 6378137*Math.acos(Math.sin(lat2)*Math.sin(lat1)+Math.cos(lat2)*Math.cos(lat1)*Math.cos(long2-long1));

        return d;
    }
}