class CalculDistance {
    
    constructor (activite) {
        console.log(this.calculDistanceTrajet(activite));
    }
    
    calculDistance2PointsGPS(lat1, long1, lat2, long2) {
        
        lat1 = Math.PI*lat1/180;
        lat2 = Math.PI*lat2/180;
        long1 = Math.PI*long1/180;
        long2 = Math.PI*long2/180;

        d = 6378137*Math.acos(Math.sin(lat2)*Math.sin(lat1)+Math.cos(lat2)*Math.cos(lat1)*Math.cos(long2-long1));

        return d;
    }

    calculDistanceTrajet(activite){
        if (typeof activite !== 'undefined') {
  
        distance = 0;
        distanceTraj = 0;
  
        for (let i = 0; i < activite.data.length-1; ++i) {
  
        lat1 = activite.data[i].latitude;
        long1 = activite.data[i].longitude;
        lat2 = activite.data[i+1].latitude;
        long2 = activite.data[i+1].longitude;
  
        distance = calculDistance2PointsGPS(lat1, long1, lat2, long2);
  
        distanceTraj = distanceTraj + distance;
  
        }} else {
            distanceTraj = -1;
            console.log("ERROR - Activite is not defined.");
        }
        return distanceTraj;
    }

}

new CalculDistance({
    "activity":{
        "date":"01/09/2018",
        "description": "IUT -> RU"
    },
    "data":[
        {"time":"13:00:00","cardio_frequency":99,"latitude":47.644795,"longitude":-2.776605,"altitude":18},
        {"time":"13:00:05","cardio_frequency":100,"latitude":47.646870,"longitude":-2.778911,"altitude":18},
        {"time":"13:00:10","cardio_frequency":102,"latitude":47.646197,"longitude":-2.780220,"altitude":18},
        {"time":"13:00:15","cardio_frequency":100,"latitude":47.646992,"longitude":-2.781068,"altitude":17},
        {"time":"13:00:20","cardio_frequency":98,"latitude":47.647867,"longitude":-2.781744,"altitude":16},
        {"time":"13:00:25","cardio_frequency":103,"latitude":47.648510,"longitude":-2.780145,"altitude":16}
    ]
})