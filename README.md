# SportTrack : Application Js

#Date
October 2021

#Décompresser l'archive
La première étape consiste a décomprésser l'archive grâce à la commande tar -czvf nom-de-l-archive.tar.gz /chemin/vers/répertoire-ou-fichier . Placer l'application dans le répertoire que vous souhaitez sur votre machine.

#Installation

cd appjs/express_webapp
npm install

cd ../sport-track-db
npm install

Vous pouvez vous rendre dans le répertoire express_webapp pour lancer le serveur

cd ../express_webapp
npm start
Pour accèder à l'application il faut se rendre sur http://localhost:3000.






